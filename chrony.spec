%global clknetsim_ver 79ffe4

Name:      chrony
Version:   3.5
Release:   3
Summary:   An NTP client/server
License:   GPLv2
URL:       https://chrony.tuxfamily.org
Source0:   https://download.tuxfamily.org/chrony/chrony-%{version}%{?prerelease}.tar.gz
Source1:   chrony.dhclient
Source2:   chrony.helper
Source3:   chrony-dnssrv@.service
Source4:   chrony-dnssrv@.timer

Source6:   https://github.com/mlichvar/clknetsim/archive/%{clknetsim_ver}/clknetsim-%{clknetsim_ver}.tar.gz

Patch0:    chrony-service-helper.patch
Patch1:	   chrony-packettest.patch  
Patch2:	   0001-main-create-new-file-when-writing-pidfile.patch
BuildRequires: gcc gcc-c++ bison systemd libcap-devel libedit-devel nettle-devel pps-tools-devel libseccomp-devel

Requires:      shadow-utils systemd timedatex

%description
chrony is a versatile implementation of the Network Time Protocol (NTP).
It can synchronise the system clock with NTP servers, reference clocks
(e.g. GPS receiver), and manual input using wristwatch and keyboard. It can
 also operate as an NTPv4 (RFC 5905) server and peer to provide a time
service to other computers in the network.

%package_help

%prep

%setup -q -n  %{name}-%{version} -a 6
%patch0 -p1
%patch1 -p1
%patch2 -p1
mv clknetsim-%{clknetsim_ver}* test/simulation/clknetsim

%build
%configure \
        --enable-debug --enable-ntp-signd --enable-scfilter --docdir=%{_docdir} \
        --with-ntp-era=$(date -d '1970-01-01 00:00:00+00:00' +'%s') \
        --with-user=chrony  --with-hwclockfile=%{_sysconfdir}/adjtime --with-sendmail=%{_sbindir}/sendmail
%make_build

%install
%make_install

install -d $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig
install -m 644 -p examples/chrony.conf.example2 $RPM_BUILD_ROOT%{_sysconfdir}/chrony.conf
install -m 640 -p examples/chrony.keys.example  $RPM_BUILD_ROOT%{_sysconfdir}/chrony.keys

install -d $RPM_BUILD_ROOT%{_sysconfdir}/NetworkManager/dispatcher.d
install -m 755 -p examples/chrony.nm-dispatcher $RPM_BUILD_ROOT%{_sysconfdir}/NetworkManager/dispatcher.d/20-chrony

install -d $RPM_BUILD_ROOT%{_sysconfdir}/dhcp/dhclient.d
install -m 755 -p %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/dhcp/dhclient.d/chrony.sh

install -d $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d
install -m 644 -p examples/chrony.logrotate  $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/chrony
cat > $RPM_BUILD_ROOT%{_sysconfdir}/sysconfig/chronyd <<EOF
# Command-line options for chronyd
OPTIONS=""
EOF

install -d $RPM_BUILD_ROOT%{_libexecdir}
install -m 755 -p %{SOURCE2} $RPM_BUILD_ROOT%{_libexecdir}/chrony-helper

install -d $RPM_BUILD_ROOT%{_unitdir}
install -m 644 -p examples/chrony-wait.service  $RPM_BUILD_ROOT%{_unitdir}/chrony-wait.service
install -m 644 -p examples/chronyd.service   $RPM_BUILD_ROOT%{_unitdir}/chronyd.service
install -m 644 -p %{SOURCE3} $RPM_BUILD_ROOT%{_unitdir}/chrony-dnssrv@.service
install -m 644 -p %{SOURCE4} $RPM_BUILD_ROOT%{_unitdir}/chrony-dnssrv@.timer


install -d $RPM_BUILD_ROOT%{_prefix}/lib/systemd/ntp-units.d
echo 'chronyd.service' >  $RPM_BUILD_ROOT%{_prefix}/lib/systemd/ntp-units.d/50-chronyd.list

install -d $RPM_BUILD_ROOT%{_localstatedir}/lib/chrony
install -d $RPM_BUILD_ROOT%{_localstatedir}/log/chrony
touch $RPM_BUILD_ROOT%{_localstatedir}/lib/chrony/{drift,rtc}

%check
%make_build -C test/simulation/clknetsim CLKNETSIM_RANDOM_SEED=24502
make quickcheck


%pre
if ! getent group chrony > /dev/null ; then
    groupadd -r chrony
fi
if ! getent passwd chrony >/dev/null ; then
    useradd -r -g chrony -d %{_localstatedir}/lib/chrony -s /sbin/nologin chrony
fi


%preun
%systemd_preun chronyd.service chrony-wait.service

%post
%systemd_post chronyd.service chrony-wait.service


%postun
%systemd_postun_with_restart chronyd.service

%files
%defattr(-,root,root)
%license COPYING

%config(noreplace) %{_sysconfdir}/chrony.conf
%config(noreplace) %verify(not md5 size mtime) %attr(640,root,chrony) %{_sysconfdir}/chrony.keys
%config(noreplace) %{_sysconfdir}/logrotate.d/chrony
%config(noreplace) %{_sysconfdir}/sysconfig/chronyd
%{_sysconfdir}/NetworkManager/dispatcher.d/20-chrony
%{_sysconfdir}/dhcp/dhclient.d/chrony.sh

%{_bindir}/chronyc
%{_sbindir}/chronyd
%{_libexecdir}/chrony-helper
%{_prefix}/lib/systemd/ntp-units.d/*.list
%{_unitdir}/chrony*.service
%{_unitdir}/chrony*.timer

%dir %attr(-,chrony,chrony) %{_localstatedir}/lib/chrony
%ghost %attr(-,chrony,chrony) %{_localstatedir}/lib/chrony/drift
%ghost %attr(-,chrony,chrony) %{_localstatedir}/lib/chrony/rtc
%dir %attr(-,chrony,chrony) %{_localstatedir}/log/chrony


%files help
%defattr(644,root,root)
%doc FAQ NEWS README
%{_mandir}/man[158]/%{name}*.[158]*

%changelog
* Tue Dec 15 2020 xihaochen <xihaochen@huawei.com> - 3.5-3
- Type:requirement
- Id:NA
- SUG:NA
- DESC:remove sensitive words 

* Sat Sep 19 2020 yuboyun <yuboyun@huawei.com> - 3.5-2
- fix CVE-2020-14367

* Sat Jun 28 2020 hanzhijun <hanzhijun1@huawei.com> - 3.5-1
- update to 3.5-1

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.4-3
- rename docs subpackage as help subpackage

* Sat Sep 14 2019 hufeng <solar.hu@huawei.com> - 3.4-2
- Create chrony spec
